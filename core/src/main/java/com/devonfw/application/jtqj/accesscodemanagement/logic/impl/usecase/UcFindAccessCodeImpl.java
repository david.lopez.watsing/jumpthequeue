package com.devonfw.application.jtqj.accesscodemanagement.logic.impl.usecase;

import java.util.Optional;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.devonfw.application.jtqj.accesscodemanagement.dataaccess.api.AccessCodeEntity;
import com.devonfw.application.jtqj.accesscodemanagement.logic.api.to.AccessCodeEto;
import com.devonfw.application.jtqj.accesscodemanagement.logic.api.to.AccessCodeSearchCriteriaTo;
import com.devonfw.application.jtqj.accesscodemanagement.logic.api.usecase.UcFindAccessCode;
import com.devonfw.application.jtqj.accesscodemanagement.logic.base.usecase.AbstractAccessCodeUc;

/**
 * Use case implementation for searching, filtering and getting AccessCodes
 */
@Named
@Validated
@Transactional
public class UcFindAccessCodeImpl extends AbstractAccessCodeUc implements UcFindAccessCode {

  /** Logger instance. */
  private static final Logger LOG = LoggerFactory.getLogger(UcFindAccessCodeImpl.class);

  @Override
  public AccessCodeEto findAccessCode(long id) {

    LOG.debug("Get AccessCode with id {} from database.", id);
    Optional<AccessCodeEntity> foundEntity = getAccessCodeRepository().findById(id);
    if (foundEntity.isPresent())
      return getBeanMapper().map(foundEntity.get(), AccessCodeEto.class);
    else
      return null;
  }

  @Override
  public Page<AccessCodeEto> findAccessCodes(AccessCodeSearchCriteriaTo criteria) {

    Page<AccessCodeEntity> accesscodes = getAccessCodeRepository().findByCriteria(criteria);
    return mapPaginatedEntityList(accesscodes, AccessCodeEto.class);
  }

  // TODO: ¿Para que este metodo si es igual que el anterior? ¿Posible evolucion de cobigen que antes no generaba?
  @Override
  public Page<AccessCodeEto> findAccessCodeEtos(AccessCodeSearchCriteriaTo criteria) {

    Page<AccessCodeEntity> accessCodes = getAccessCodeRepository().findByCriteria(criteria);
    return mapPaginatedEntityList(accessCodes, AccessCodeEto.class);
  }

}
